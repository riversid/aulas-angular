import { Component, OnInit, Input } from '@angular/core';
import {Student} from './student.model'

@Component({
  selector: 'jad-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  @Input() student: Student
  // @Input() name:string = "Osias"
  // @Input() isJedi:boolean = false

  constructor() { }

  ngOnInit() {
  }

  clickme():void {
    this.student.isJedi = !this.student.isJedi
  }
}
