// Aula 1
let message:string = "Help me, Obi-Wan Kenobi"
console.log(message)

let episode:number = 4
console.log("Episode " + episode)

episode++
console.log("Next is " + episode)

let droid = "BB-8"
console.log("Droid: " + droid)

// Aula 2
let isEnoughToBeatMF = function (parsecs: number) : boolean {
    return parsecs < 12
}

let distance = 11 //14
console.log(`Is ${distance} parsecs enough to beat Millenium Falcon?  ${isEnoughToBeatMF(distance)?"YES":"NO"}`)

let call = (name:string) => console.log(`Do you copy ${name}?`)
call("Gold leader")

//function inc(speed:number, inc:number):number { // <-- isso retorna NaN se não colocar o segundo parametro
function inc(speed:number, inc:number=1):number { // <-- isso retorna 
    return speed + inc
}

console.log(`inc(5,1) = ${ inc(5,1) }`)
console.log(`inc(5) = ${ inc(5) }`)
