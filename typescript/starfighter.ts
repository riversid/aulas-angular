import {Spacecraft, Containership} from './base-ships'

// classe com herança e interfaces
export class MilleniumFalcon extends Spacecraft implements Containership{

    cargoContainers:number

    constructor() {
        super('hyperdrive') // equivale a instanciar a SpaceCraft com o valor passado (new Spacecraft('...'))
        this.cargoContainers = 2
    }

    jumpIntoHyperSpace(){
        let prob:number = 0.5
        console.log(prob)
        if (Math.random() >= prob){
            super.jumpIntoHyperSpace()
        }
        else {
            console.log('Failed to jump into Hyperspace')
        }
    }
}