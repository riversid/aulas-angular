// Aula 1
var message = "Help me, Obi-Wan Kenobi";
console.log(message);
var episode = 4;
console.log("Episode " + episode);
episode++;
console.log("Next is " + episode);
var droid = "BB-8";
console.log("Droid: " + droid);
// Aula 2
var isEnoughToBeatMF = function (parsecs) {
    return parsecs < 12;
};
var distance = 11; //14
console.log("Is " + distance + " parsecs enough to beat Millenium Falcon?  " + (isEnoughToBeatMF(distance) ? "YES" : "NO"));
var call = function (name) { return console.log("Do you copy " + name + "?"); };
call("Gold leader");
//function inc(speed:number, inc:number):number { // <-- isso retorna NaN se não colocar o segundo parametro
function inc(speed, inc) {
    if (inc === void 0) { inc = 1; }
    return speed + inc;
}
console.log("inc(5,1) = " + inc(5, 1));
console.log("inc(5) = " + inc(5));
