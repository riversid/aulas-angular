import { Spacecraft, Containership } from './base-ships'
import { MilleniumFalcon } from './starfighter'

let ship = new Spacecraft('hyperdriven')
ship.jumpIntoHyperSpace()

let falcon = new MilleniumFalcon()
falcon.jumpIntoHyperSpace()

let goodForTheJob = (ship : Containership) => ship.cargoContainers > 2
console.log(`Is falcon good for the job? ${goodForTheJob(falcon) ? "YES" : "NO" }`)