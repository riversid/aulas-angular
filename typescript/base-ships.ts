// criando classe
class Spacecraft {
    constructor(public propulsor:string){ }

    jumpIntoHyperSpace(){
        console.log(`Entering hyperspace with ${this.propulsor}`)
    }
}

// interfaces
interface Containership {
    cargoContainers: number
    //cargoContainers?: number // <<< o '?' indica que o atributo é opcional
}

export { Spacecraft, Containership }