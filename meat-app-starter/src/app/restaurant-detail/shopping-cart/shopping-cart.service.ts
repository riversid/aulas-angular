import { ShoppingCart } from "./shopping-cart.model"
import { MenuItem } from "../menu-item/menu-item.model"

export class ShoppingCartService {
    items: ShoppingCart[] = []

    clear(){
        this.items = []
    }

    addItem(item: MenuItem){
        let foundItem = this.items.find((mItem) => mItem.menuItem.id === item.id)
        if(foundItem){
            foundItem.quantity++
        }
        else {
            this.items.push(new ShoppingCart(item))
        }
    }

    removeItem(item: ShoppingCart){
        this.items.splice(this.items.indexOf(item), 1)
    }

    total(): number {

        return this.items
            .map(item => item.value())
            .reduce((prev, value) => prev+value, 0)
    }
}